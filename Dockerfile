FROM guardicore/monkey-island:1.11.0

USER root
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get --allow-releaseinfo-change update && \
	apt-get install -y gnupg2

RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add -
RUN echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.2 main" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list
RUN apt-get update && \
	apt-get install -y mongodb-org

RUN mkdir -p /monkey/monkey_island/bin/mongodb/bin/ && \
	ln -sf /usr/bin/mongod /monkey/monkey_island/bin/mongodb/bin/
RUN sed 's/false/true/' -i /monkey/monkey_island/cc/server_config.json

USER monkey-island
